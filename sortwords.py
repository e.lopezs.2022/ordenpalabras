'''
Program to order a list of words given as arguments
'''
#import sys

def is_lower(first: str, second: str):
    """Return True if first is lower (alphabetically) than second

    Order is checked after lowercasing the letters
    `<` is only used on single characteres.
    """
    if len(first) > len(second):
        a = len(second)
    else:
        a = len(first)
    for i in range(a):
        if first[i].lower()!=second[i].lower():
            if first[i].lower()<second[i].lower():
                return True
            else:
                return False


def get_lower(words: list, pos: int):
    """Get lower word, for words right of pos (including pos)"""
    Words = words[pos:]
    a = ""
    menor = Words[0]
    mayor = Words[0]
    for i in Words:
        if is_lower(i, menor) == False:
            menor = i
    for i in range(len(words)):
        if words[i] == menor:
            return i


def sort(words: list):
    """Return the list of words, ordered alphabetically"""
    lista = []
    for i in range(len(words)):
        quitar = get_lower(words, 0)
        lista.insert(0, words[quitar])
        words.remove(words[quitar])
    return lista


def show(words: list):
    """Show words on screen, using print()"""
    imprimir=sort(words)
    texto=" ".join(imprimir)
    print(texto)


def main():
    #words: list = sys.argv[1:]
    ordered: list = sort(words)
    show(ordered)

words=['hola','adios','voy','vengo']
if __name__ == '__main__':
    main()



